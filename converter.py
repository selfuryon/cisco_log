import glob

from pygrok import Grok

# Начальные данные
log_files = glob.glob("logs/*")
pattern = "%{CISCOTIMESTAMP:log_date_server} %{DATA:hostname_server} (\d+ ?)?: (%{INT:year} )?[\*\.]?" \
          "%{CISCOTIMESTAMP:log_date}( %{DATA:log_timezone})?:( %{DATA:hostname}:)?\s+\*?%%{DATA:facility}-" \
          "%{INT:severity}-%{DATA:mnemonic}:%{GREEDYDATA:message}"
pattern_event_num = "message repeated %{INT:num} time"
grok = Grok(pattern)
grok_event_num = Grok(pattern_event_num)

# Конвертим данные в один большой файл
with open("output.csv", encoding='utf-8', mode='w') as f_out:
    columns = "log_date_server\thostname_server\tfacility\tseverity\tmnemonic\tmessage\tevent_nums\n"
    f_out.write(columns)
    for log_file in log_files:
        with open(log_file, 'r') as f_in:
            previous_object = {}
            for current_line in f_in:
                current_object = {}
                parsed_data = grok.match(current_line)
                if parsed_data is None:
                    parsed_event_num = grok_event_num.match(current_line)
                    if bool(parsed_event_num) & bool(previous_object):
                        previous_object['event_nums'] += int(parsed_event_num['num'])
                        continue
                    else:
                        print(current_line)
                else:
                    current_object['log_date_server'] = parsed_data.get('log_date_server', '')
                    current_object['hostname_server'] = parsed_data.get('hostname_server', '')
                    # current_object['log_date'] = parsed_data.get('log_date', '')
                    # current_object['log_timezone'] = parsed_data.get('log_timezone', '')
                    # current_object['hostname'] = parsed_data.get('hostname', '')
                    current_object['facility'] = parsed_data.get('facility', '')
                    current_object['severity'] = parsed_data.get('severity', '')
                    current_object['mnemonic'] = parsed_data.get('mnemonic', '')
                    current_object['message'] = parsed_data.get('message', '')
                    current_object['event_nums'] = 1
                    message = current_object['message']
                    parsed_event_num = grok_event_num.match(message)
                    if parsed_event_num:
                        current_object['event_nums'] = int(parsed_event_num['num'])

                if previous_object:
                    row = "{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(previous_object['log_date_server'],
                                                                previous_object['hostname_server'],
                                                                # previous_object['log_date'],
                                                                # previous_object['log_timezone'],
                                                                # previous_object['hostname'],
                                                                previous_object['facility'],
                                                                previous_object['severity'],
                                                                previous_object['mnemonic'],
                                                                previous_object['message'],
                                                                previous_object['event_nums'], )
                    f_out.write(row)

                previous_object = current_object
            if previous_object:
                row = "{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(previous_object['log_date_server'],
                                                            previous_object['hostname_server'],
                                                            # previous_object['log_date'],
                                                            # previous_object['log_timezone'],
                                                            # previous_object['hostname'],
                                                            previous_object['facility'],
                                                            previous_object['severity'],
                                                            previous_object['mnemonic'],
                                                            previous_object['message'],
                                                            previous_object['event_nums'], )
                f_out.write(row)

print("Данные конвертированы в один файл")
